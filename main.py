
import board
import busio
from time import sleep
from digitalio import Direction, Pull
from adafruit_mcp230xx.mcp23017 import MCP23017
import rotaryio
import board
import usb_hid
from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
from adafruit_hid.keycode import Keycode

keyboard = Keyboard(usb_hid.devices)
keyboard_layout = KeyboardLayoutUS(keyboard)

#encoder = rotaryio.IncrementalEncoder(board.D10, board.D11)
#last_position = None

i2c = busio.I2C(board.SCL, board.SDA)
mcp1 = MCP23017(i2c, address=0x21)
mcp2 = MCP23017(i2c, address=0x22)
mcp3 = MCP23017(i2c, address=0x23)

mcp1_port_a_pins = []
for pin in range(0, 8):
    mcp1_port_a_pins.append(mcp1.get_pin(pin))
mcp1_port_b_pins = []
for pin in range(8, 16):
    mcp1_port_b_pins.append(mcp1.get_pin(pin))
for pin in mcp1_port_a_pins:
    pin.direction = Direction.INPUT
    pin.pull = Pull.UP
for pin in mcp1_port_b_pins:
    pin.direction = Direction.INPUT
    pin.pull = Pull.UP

mcp2_port_a_pins = []
for pin in range(0, 8):
    mcp2_port_a_pins.append(mcp2.get_pin(pin))
mcp2_port_b_pins = []
for pin in range(8, 16):
    mcp2_port_b_pins.append(mcp2.get_pin(pin))
for pin in mcp2_port_a_pins:
    pin.direction = Direction.INPUT
    pin.pull = Pull.UP
for pin in mcp2_port_b_pins:
    pin.direction = Direction.INPUT
    pin.pull = Pull.UP

mcp3_port_a_pins = []
for pin in range(0, 8):
    mcp3_port_a_pins.append(mcp3.get_pin(pin))
mcp3_port_b_pins = []
for pin in range(8, 16):
    mcp3_port_b_pins.append(mcp3.get_pin(pin))
for pin in mcp3_port_a_pins:
    pin.direction = Direction.INPUT
    pin.pull = Pull.UP
for pin in mcp3_port_b_pins:
    pin.direction = Direction.INPUT
    pin.pull = Pull.UP
key_types = ['toggle', 'repeat']
keymap = {
    '21A0' : (Keycode.A, Keycode.SHIFT, 'toggle'),
}
state = {}
while True:
    keymux = None
    for num, button in enumerate(mcp1_port_a_pins):
        if not button.value:
            #print("mcp1 A Button #", num, "pressed!")
            keymux = '21A' + str(num)
    for num, button in enumerate(mcp1_port_b_pins):
        if not button.value:
            #print("mcp1 B Button #", num, "pressed!")
            keymux = '21B' + str(num)
    for num, button in enumerate(mcp2_port_a_pins):
        if not button.value:
            #print("mcp2 A Button #", num, "pressed!")
            keymux = '22A' + str(num)
    for num, button in enumerate(mcp2_port_b_pins):
        if not button.value:
            #print("mcp2 B Button #", num, "pressed!")
            keymux = '22B' + str(num)
    for num, button in enumerate(mcp3_port_a_pins):
        if not button.value:
            #print("mcp3 A Button #", num, "pressed!")
            keymux = '23A' + str(num)
    for num, button in enumerate(mcp3_port_b_pins):
        if not button.value:
            #print("mcp3 B Button #", num, "pressed!")
            keymux = '23B' + str(num)
    #position = encoder.position
    #if last_position is None or position != last_position:
    #    print(position)
    # last_position = position
    if keymux == None:
        keyboard.release_all()
    else:
        action = keymap.get(keymux, (Keycode.PERIOD, 0, 'repeat'))
        if state.get(keymux, False) and action[2] == 'repeat' :
            keyboard.press(action[1], action[0])
        else:
            state.update({keymux : True})
            keyboard.press(action[1], action[0])
        print(keymux)
      # "Press"...
    sleep(0.05)
