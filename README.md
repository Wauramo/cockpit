# Cockpit

Flight simulator cockpit button panel

## Getting started

## buttons

- toggle pitot heat / switch 10
- toggle anti ice / switch 11

- toggle water rudder / switch

- togle parking brakes / switch
- set autobrake LO / switch
- set autobrake MED / switch
- set aurobrake HI / switch
- set autobrake control / switch
- right brake / button
- left brake / button
- brakes / switch

- Toggle master ignition switch / switch
- engine autostop / button
- engine autostart / button
- increase cowl flaps / button
- decrease cwol flaps / button

- toggle master battery / switch 2
- toggle master alternator / switch 3

- toggle auto rudder / switch

- toggle tail wheel lock / switch
- toggle landing gear / switch

- toggle fuel dump / switch
- toggle all fuel valves / switch

- display navlog / button 4
- display map / button 5
- display checklist / button 6
- toggle basic control panel / button 7
- next toolbar panel / button 8
- previous toolbar panel / button 9

- toggle throttle reverse / button
- throttle cut / switch

- toggle spoilers / switch
- retract flaps / encoder
- increase flpas / encoder
- extend flaps / encoder
- decrease flaps / encoder

- toggle avionics master / switch 1